import React from "react";
import styled from "styled-components";
import { LocalMessage, State } from "../types";
import { BiCheckDouble, BiCheck } from "react-icons/bi";
import { GrUploadOption } from "react-icons/gr";
import {
  format,
  differenceInHours,
  differenceInSeconds,
  parseISO,
  isToday,
  isYesterday,
} from "date-fns";

const TimeLabel: React.FC<{
  date: Date;
}> = ({ date }) => {
   const formatDay = (date: Date) => {
    if (isToday(date)) {
      return "Today";
    } else if (isYesterday(date)) {
      return "Yesterday";
    } else {
      return format(date, "MMMM d, yyyy");
    }
  };
  
  return (
    <TimeLabelWrapper>
      <Day>{formatDay(date)}</Day>
      <Time>{format(date, "h:mm a")}</Time>
    </TimeLabelWrapper>
  );
};

export default TimeLabel;

const TimeLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const Day = styled.div`
  font-weight: bold;
  margin: 5px;
  color: #7d7d7d;
`

const Time = styled.div`
  margin: 5px;
  color: #919090;

`