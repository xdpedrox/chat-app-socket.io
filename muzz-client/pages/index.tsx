import Image from "next/image";
import { Inter } from "next/font/google";
import ChatPage from "../components/Chat/Chat";
import Link from "next/link";
import styled from "styled-components";

export default function Home() {
  return (
    <Container>
      <PageContent>
        <Title>Muzz Exercise</Title>

        <ButtonContainer>
          <Link href={{ pathname: "/1/1" }}>
            <Button>Chat as Aafa</Button>
          </Link>

          <Link href={{ pathname: "/2/1" }}>
            <Button>Chat as Aalif</Button>
          </Link>
        </ButtonContainer>
      </PageContent>
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  /* background-color: red; */
`;

const Title = styled.h1`
  font-weight: bold;
  font-size: xx-large;
  justify-self: center;
`;

const PageContent = styled.div`
  height: 400px;
  /* background-color: green; */
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  /* background-color: yellow; */
  margin-top: 100px;
`;
const Button = styled.button`
  margin: 40px;
  background-color: #e9506f;

  border: none;
  border-radius: 20px; /* Adjust the value to control the roundness */
  padding: 10px 20px; /* Adjust the padding as needed */
  color: #ffffff; /* Change the text color */
  cursor: pointer;
`;
