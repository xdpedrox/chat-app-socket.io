# Muzz - Full Stack Exercise Overview

## Project Scope

The present project, while illustrative of key functionalities, is not a comprehensive application and lacks certain essential elements, such as:

- Authentication
- Socket restriction: All messages are currently broadcasted to all users connected to the socket server.

## Key Features

Within its current framework, the project incorporates the following features:

- Web socket communication for facilitating real-time messaging.
- A "Seen" feature, notifying users when their messages have been received by the intended recipient.
- Implementation of a REST API to retrieve historical messages.
- Implementation of a database for storing and managing sent messages.

## Known Issues

It is important to note the existence of certain bugs in the system:

- The scroll position is not maintained at the bottom when sending a new message, causing an inconvenience during user interaction.
- In scenarios where a user has multiple tabs open for the same account, the second tab fails to receive messages.

Solving these issues is possible with additional time and development efforts.

# Install



## Client
```
npm install
npm run dev
```


## Client
```
npm install
npx prisma generate
npm run dev
```