import React from "react";
import styled from "styled-components";
import { LocalMessage, State } from "../types";

const Header: React.FC<{
  state: State;
}> = ({ state }) => {
  return (
    <Container>
      <ImageWrapper>
        <Image src={state.toUser?.profileUrl} alt={state.toUser?.name} />
      </ImageWrapper>
      <Name>{state.toUser?.name}</Name>
    </Container>
  );
};

export default Header;

const Container = styled.div`
  padding: 10px;
  display: flex;
  height: 70px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  text-align: center;
  border-bottom: 2px solid black;

`;

const Image = styled.img`
  width: 100%; /* Make the image fill the container */
  height: auto;
  border-radius: 50%;
`;

const Name = styled.h2`

`;

const ImageWrapper = styled.div`
  width: 55px; /* Adjust the width and height as needed */
  height: 55px;
  overflow: hidden;
  border-radius: 50%;
  margin: 10px;
`;
