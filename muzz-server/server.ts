// src/server.ts
import express, { Request, Response } from "express";
import http from "http";
import { Server, Socket } from "socket.io";
import db from "./prisma";
import cors from "cors";
import bodyParser from "body-parser";

interface MessageWebSocket {
  temporaryId: string;
  fromUserId: number;
  chatId: number;
  message: string;
  sentAt: Date;
}

interface MessagesRequestSocket {
  messageCursorId?: number;
  fromUserId: number;
  chatId: number;
  numberOfItems: number;
}

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: { origin: "*" },
});

// Serve static files from the 'public' directory
app.use(express.static("public"));

app.use(cors({ origin: "*" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post(
  "/chat",
  async (req: Request<{}, {}, MessagesRequestSocket>, res: Response) => {
    try {
      const data = req.body;
      // console.log("data", req.body)
      // Make a request using Axios

      const chat = await db.chat.findFirst({
        where: { id: data.chatId, users: { some: { id: data.fromUserId } } },
        include: {
          users: true,
        },
      });

      // console.log("chat", chat)
      if (chat) {
        res.json(chat);
      }

      // Send the response data as JSON
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  }
);

app.post(
  "/messages",
  async (req: Request<{}, {}, MessagesRequestSocket>, res: Response) => {
    try {
      const data = req.body;
      // console.log("data", req.body)
      // Make a request using Axios
      const chat = await db.chat.findFirst({
        where: { id: data.chatId, users: { some: { id: data.fromUserId } } },
        include: {
          users: true,
          messages: {
            take: data.numberOfItems,
            ...(data.messageCursorId
              ? {
                  cursor: { id: data.messageCursorId },
                  skip: 1,
                }
              : {}),
            orderBy: {
              id: "desc",
            },
          },
        },
      });

      if (chat) {
        res.json({ messages: chat.messages.reverse() });
      }

      // Send the response data as JSON
    } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
    }
  }
);

// Handle incoming socket connections
io.on("connection", (socket: Socket) => {
  // // console.log("A user connected");
  // console.log("connected:" + socket.id);

  // Handle chat messages
  socket.on("message-client-to-server", async (data: MessageWebSocket) => {
    try {
      // console.log("Message Received");
      const chat = await db.chat.findFirst({
        where: { id: data.chatId, users: { some: { id: data.fromUserId } } },
        include: { users: true },
      });

      if (chat) {
        const savedMessage = await db.message.create({
          data: {
            message: data.message,
            chatId: data.chatId,
            fromUserId: data.fromUserId,
          },
        });
        // Broadcast the message to all connected clients
        io.emit("message-server-to-client", {
          ...savedMessage,
          temporaryId: data.temporaryId,
        });
        // console.log("Chat found Message Saved");
      } else {
        throw "Chat not found or invalid user";
      }
    } catch (err) {
      // console.log(err);
    }
  });

  // Handle chat messages
  socket.on(
    "message-seen-client-to-server",
    async (data: {
      ids: number[];
      seenByUserId: number;
      messageFromUserId: number;
      chatId: number;
    }) => {
      try {
        // console.log("data.fromUserId1", data.fromUserId)

        if (!data.seenByUserId && !data.messageFromUserId && !data.chatId)
          return "fromUserId or chatId not found.";

        const chat = await db.chat.findFirst({
          where: { 
            id: data.chatId, 
            users: { some: { id: data.messageFromUserId  } } 
          },
          include: {
            users: true,
            messages: { where: { id: { in: data.ids } } },
          },
        });
        console.log("Message seen received", data);

        if (chat && chat.messages.length != 0) {
          const messages = await db.message.updateMany({
            where: { id: { in: data.ids } },
            data: { seen: true },
          });

          // Broadcast the message to all connected clients
          // console.log("data.fromUserId", data.fromUserId)
          io.emit("message-seen-server-to-client", {
            seenByUserId: data.seenByUserId,
            messageFromUserId: data.messageFromUserId,
            messageIds: data.ids,
            chatId: data.chatId
          });
          console.log("Message seen updated");
        } else {
          throw "Chat not found or invalid user or no messages to update";
        }
      } catch (err) {
        console.error(err);
      }
    }
  );

  // Handle disconnection
  socket.on("disconnect", () => {
    console.log("User disconnected");
  });
});

// Start the server on port 3000
const PORT = process.env.PORT || 3001;
server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
