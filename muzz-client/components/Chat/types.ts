export interface ReceiveMessageSocketIo {
  id: number;
  temporaryId: string;
  chatId: number;
  fromUserId: number;
  message: string;
  seen: boolean;
  sent: true;
  createdAt: Date;
  updatedAt: Date;
}

export type Chat = {
  id: number;
  createdAt: Date;
};

export interface MessageNotSent {
  id: string;
  chatId: number;
  fromUserId: number;
  message: string;
  sent: false;
  seen: false;
  createdAt: Date;
}

export interface MessageSent {
  id: number;
  chatId: number;
  fromUserId: number;
  message: string;
  sent: true;
  seen: false;
  createdAt: Date;
}

export interface MessageSeen {
  id: number;
  chatId: number;
  fromUserId: number;
  message: string;
  sent: true;
  seen: true;
  createdAt: Date;
}

export type MessagesServer = MessageSent | MessageSeen;
export type LocalMessage = MessageNotSent | MessagesServer;

// Define the state and action types
export type State = {
  user: User | null;
  toUser: User | null;
  chat: Chat | null;
  messages: LocalMessage[];
  lowestMessageId: number | null
  hasMore: boolean
};

export interface MessagesRequestSocket {
  messageCursorId?: number;
  fromUserId: number;
  chatId: number;
  numberOfItems: number;
}

export type User = { id: number, name: string, profileUrl: string }


export interface ChatRequest {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  users: User[];
}