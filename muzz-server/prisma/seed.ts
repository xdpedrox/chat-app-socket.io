import db from "./index"

const seed = async () => {
    const chat = await db.chat.create({data: {id: 1}})

    const user1 = await db.user.create({data: {id: 1, name: "Aalif", chats: {connect: {id: chat.id}}}})
    const user2 = await db.user.create({data: {id: 2, name: "Aafa", chats: {connect: {id: chat.id}}}})
  }
  
seed()
  