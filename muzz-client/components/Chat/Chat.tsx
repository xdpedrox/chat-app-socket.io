import useChat from "./useChat";
import React, { useRef, useState, useEffect } from "react";
import styled from "styled-components";
import MessageBubble from "./components/MessageBubble";
import {
  format,
  differenceInHours,
  differenceInSeconds,
  parseISO,
  isToday,
  isYesterday,
} from "date-fns";
import TimeLabel from "./components/TimeLabel";
import Header from "./components/Header";
import InfiniteScroll from "react-infinite-scroller";

const Chat: React.FC = () => {
  const {
    state,
    messageInput,
    setMessageInput,
    handleSendMessage,
    loadMessages,
  } = useChat();

  return (
    <Container>
      <Header state={state} />
      <PageContent>
        <InfiniteScroll
          className="inf-scroll"
          pageStart={0}
          loadMore={loadMessages}
          hasMore={state.hasMore}
          isReverse={true}
          loader={
            <div className="loader" key={0}>
              Loading ...
            </div>
          }
          useWindow={false}
        >
          {!state.hasMore && state?.chat?.createdAt && (
            <>
              <TimeLabel date={state?.chat?.createdAt} />
              <MatchedDiv>You Matched 🎈</MatchedDiv>
            </>
          )}
          {state.messages.map((msg, index, array) => {
            const previousMessage = index > 0 ? array[index - 1] : null;
            const nextMessage =
              index < array.length - 1 ? array[index + 1] : null;

            // Display Time or not
            const isSent1HourAfterLastMessage =
              !previousMessage ||
              differenceInHours(msg.createdAt, previousMessage.createdAt) > 1;

            return (
              <>
                {isSent1HourAfterLastMessage && (
                  <TimeLabel date={msg.createdAt} />
                )}

                <MessageBubble
                  key={msg.id}
                  state={state}
                  previousMessage={previousMessage}
                  message={msg}
                  nextMessage={nextMessage}
                />
              </>
            );
          })}
        </InfiniteScroll>
      </PageContent>
      <InputContainer>
        {/* <form onSubmit={handleSendMessage}> */}
        <Input
          type="text"
          value={messageInput}
          onChange={(e) => setMessageInput(e.target.value)}
          placeholder="Type your message..."
          onKeyDown={(event) => {
            if (event.key === "Enter") {
              // Run your function here
              handleSendMessage();
            }
          }}
        />

        <button onClick={() => loadMessages()}>Load Messages</button>
      </InputContainer>
    </Container>
  );
};

export default Chat;

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const PageContent = styled.div`
  /* width: 100%; */
  flex: 1;
  overflow: auto;
  flex-direction: column-reverse;
  /* display: flex; */
`;

const InputContainer = styled.div`
  border-top: 1px solid black;
`;

const Input = styled.input`
  width: calc(100% - 60px);
  margin: 20px;
  border-radius: 100px; /* Rounded corners on the left */
  border: 1px solid #ccc; /* Border for visual clarity */
  outline: none; /* Remove default input outline */
  padding: 10px;
`;

const MatchedDiv = styled.div`
  width: 100%;
  text-align: center;
  margin: 15px;
  font-size: xx-large;
  font-weight: bold;
`;
