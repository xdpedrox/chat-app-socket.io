import React, {
  useState,
  useEffect,
  useReducer,
  useCallback,
  useRef,
} from "react";
import io from "socket.io-client";
import { useRouter } from "next/router";
import { nanoid } from "nanoid";
import axios from "axios";
import {
  Chat,
  LocalMessage,
  MessageNotSent,
  MessagesServer,
  ReceiveMessageSocketIo,
  State,
  MessagesRequestSocket,
  User,
  ChatRequest,
} from "./types";

const LOAD_CHAT = "LOAD_CHAT";
const LOAD_MESSAGES = "LOAD_MESSAGES";
const SET_HAS_MORE = "SET_HAS_MORE";
const ADD_MESSAGE = "ADD_MESSAGE";
const RECEIVE_MESSAGE = "RECEIVE_MESSAGE";
const LOAD_USER = "LOAD_USER";
const MESSAGE_SEEN = "MESSAGE_SEEN";
const MESSAGE_SENT = "MESSAGE_SENT";

type Action =
  | { type: typeof LOAD_CHAT; chat: Chat; user: User; toUser: User }
  | { type: typeof SET_HAS_MORE; hasMore: boolean }
  | {
      type: typeof LOAD_MESSAGES;
      messages: MessagesServer[];
      lowestMessageId: number;
    }
  | { type: typeof ADD_MESSAGE; message: MessageNotSent | LocalMessage }
  | { type: typeof RECEIVE_MESSAGE; message: MessagesServer }
  | { type: typeof MESSAGE_SEEN; messageId: number }
  | { type: typeof MESSAGE_SENT; temporaryId: string; serverMessageId: number };

const serverUrl = "http://localhost:3001";
const socket = io(serverUrl, {
  reconnection: true,
  reconnectionAttempts: Infinity,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
});

const instanciateDates = (m: LocalMessage) => ({
  ...m,
  createdAt: new Date(m.createdAt),
});

// Reducer function
const chatReducer = (state: State, action: Action): State => {
  // console.log("action", action);
  switch (action.type) {
    // Load page Info
    case LOAD_CHAT:
      return {
        ...state,
        chat: action.chat,
        user: action.user,
        toUser: action.toUser,
      };

    // Load Initial Message and when you scroll up
    case LOAD_MESSAGES:
      return {
        ...state,
        messages: [
          ...action.messages.map((m) => instanciateDates(m)),
          ...state.messages,
        ],
        lowestMessageId: action.lowestMessageId,
      };
    case SET_HAS_MORE:
      return {
        ...state,
        hasMore: action.hasMore,
      };
    // Used Locally when you add a message and when a message is received fom the server.
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, instanciateDates(action.message)],
      };
    // Set a message as Seen - The server has acknoledged that the Other user has received it.
    case MESSAGE_SEEN:
      // console.log("action", action)
      const messagesSeen = [...state.messages];
      let messageSeenIndex = messagesSeen.findIndex(
        (m) => m.id == action.messageId
      );
      if (messageSeenIndex != -1) {
        messagesSeen[messageSeenIndex].seen = true;
      } else {
        // console.log("Message seen not found");
      }
      return { ...state, messages: messagesSeen };
    // Set a message as sent - The server has acknoledged that it has received it.
    case MESSAGE_SENT:
      const messagesSent = [...state.messages];
      let messageSentIndex = messagesSent.findIndex(
        (m) => m.id == action.temporaryId
      );
      if (messageSentIndex != -1) {
        messagesSent[messageSentIndex].id = action.serverMessageId;
        messagesSent[messageSentIndex].sent = true;
      } else {
        // console.log("Message sent not found");
      }
      return { ...state, messages: messagesSent };
    default:
      return state;
  }
};

const useChat = () => {
  const router = useRouter();
  const { fromUserId: fromUserIdQuery, chatId: chatIdQuery } = router.query;

  const [messageInput, setMessageInput] = useState("");

  const [state, dispatch] = useReducer(chatReducer, {
    user: null,
    toUser: null,
    chat: null,
    messages: [],
    lowestMessageId: null,
    hasMore: true,
  });

  console.log("state", state);

  const loadChat = useCallback(async (chatId: number, userId: number) => {
    // console.log("tes1")
    const response = await axios.post<ChatRequest>(
      serverUrl + "/chat",
      {
        chatId: chatId,
        fromUserId: userId,
      },
      { timeout: 10000 }
    );

    const { data } = response;
    const currentUser = data.users.find((u) => u.id == userId);
    const toUser = data.users.find((u) => u.id != userId);

    if (response.status == 200 && currentUser && toUser) {
      dispatch({
        type: "LOAD_CHAT",
        chat: { id: data.id, createdAt: new Date(data.createdAt) },
        user: currentUser,
        toUser: toUser,
      });
    }
  }, []);

  useEffect(() => {
    if (chatIdQuery && fromUserIdQuery) {
      loadChat(+chatIdQuery, +fromUserIdQuery);
    }
  }, [fromUserIdQuery, chatIdQuery, loadChat]);

  const [isLoading, setIsLoading] = useState(false);

  // Load previous messages & and marks any not seen messages as seen.
  const loadMessages = async (numberOfItems?: number) => {
    const chatId = state.chat?.id;
    const userId = state.user?.id;

    if (chatId && userId && !isLoading) {
      setIsLoading(true);
      const messagesRequest: MessagesRequestSocket = {
        chatId: chatId,
        fromUserId: userId,
        numberOfItems: numberOfItems || 10,
        messageCursorId: state.lowestMessageId || undefined,
      };
      const response = await axios.post<{ messages: MessagesServer[] }>(
        serverUrl + "/messages",
        messagesRequest
      );
      // .catch(err => // console.log(err))

      setIsLoading(false);
      console.log("response", response);

      if (response.status === 200) {
        const messages = response.data.messages;
        if (messages.length > 0) {
          dispatch({
            type: LOAD_MESSAGES,
            messages: messages,
            lowestMessageId: messages[0].id,
          });
          // If the user was offline and didn't see the message make them see it.
          const notSeenMessages = messages.filter(
            (m) => !m.seen && m.fromUserId == state.toUser?.id
          );

          console.log("notSeenMessages", notSeenMessages);
          if (notSeenMessages.length == 0) return "no Messages";

          socket.emit("message-seen-client-to-server", {
            seenByUserId: state.user?.id,
            messageFromUserId: state.toUser?.id,
            chatId: chatId,
            ids: notSeenMessages.map((nsm) => nsm.id),
            loadMessages: true,
          });
        } else {
          dispatch({ type: SET_HAS_MORE, hasMore: false });
          console.log("No more messages");
        }
      }
      // console.log("response", response);
    }
  };

  useEffect(() => {
    loadMessages(20);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.chat?.id, state.user?.id]);

  // Code that handles sending and receiving and seen.
  useEffect(() => {
    // Listen for incoming messages from the server
    socket.on("message-server-to-client", (data: ReceiveMessageSocketIo) => {
      // console.log(data.fromUserId == state.user?.id);
      if (data.fromUserId == state.user?.id) {
        dispatch({
          type: MESSAGE_SENT,
          temporaryId: data.temporaryId,
          serverMessageId: data.id,
        });
      } else {
        dispatch({
          type: ADD_MESSAGE,
          message: data,
        });
        socket.emit("message-seen-client-to-server", {
          seenByUserId: state.user?.id,
          messageFromUserId: state.toUser?.id,
          chatId: state.chat?.id,
          ids: [data.id],
        });
      }
    });

    // Message Seen stuff
    socket.on(
      "message-seen-server-to-client",
      (data: {
        seenByUserId: number;
        messageFromUserId: number;
        chatId: number;
        messageIds: number[];
      }) => {
        // console.log("Received message received", data);
        data.messageIds.map((d) => {
          if (data.messageFromUserId === state.user?.id) {
            dispatch({ type: MESSAGE_SEEN, messageId: d });
          }
        });
      }
    );

    // Clean up the socket connection when the component unmounts
    return () => {
      socket.off("message-server-to-client");
      socket.off("message-seen-server-to-client");
    };
  }, [state]);

  // Code that run when you send the message
  const handleSendMessage = () => {
    if (state.chat && state.user?.id) {
      if (messageInput.trim() !== "") {
        // Send the message to the server
        // console.log("adding");
        const temporaryId = nanoid(8);
        dispatch({
          type: ADD_MESSAGE,
          message: {
            id: temporaryId,
            chatId: state.chat.id,
            fromUserId: state.user?.id,
            message: messageInput,
            seen: false,
            sent: false,
            createdAt: new Date(),
          },
        });

        socket.emit("message-client-to-server", {
          temporaryId: temporaryId,
          fromUserId: state.user?.id,
          chatId: state.chat.id,
          message: messageInput,
          sentAt: new Date(),
        });
        setMessageInput("");
      }
    }
  };

  return {
    state,
    isLoading,
    messageInput,
    setMessageInput,
    handleSendMessage,
    loadMessages,
  };
};

export default useChat;
