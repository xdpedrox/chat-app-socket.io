import React from "react";
import styled from "styled-components";
import { LocalMessage, State } from "../types";
import { BiCheckDouble, BiCheck } from "react-icons/bi";
import { GrUploadOption } from "react-icons/gr";
import {
  format,
  differenceInHours,
  differenceInSeconds,
  parseISO,
  isToday,
  isYesterday,
} from "date-fns";

const MessageBubble: React.FC<{
  state: State;
  message: LocalMessage;
  previousMessage: LocalMessage | null;
  nextMessage: LocalMessage | null;
}> = ({ state, message, previousMessage, nextMessage }) => {
  const isLastMessageFromSameOwner =
    !!previousMessage && previousMessage.fromUserId === message.fromUserId;

  const wasSent20SecAfterLastMessage =
    !!previousMessage &&
    differenceInSeconds(message.createdAt, previousMessage.createdAt) <= 20;

  const smallerMarginTop =
    isLastMessageFromSameOwner && wasSent20SecAfterLastMessage;

  // Next Message
  const isNextMessageFromSameOwner =
    !!nextMessage && nextMessage.fromUserId === message.fromUserId;

  const isNextMessageIn20Sec =
    !!nextMessage &&
    differenceInSeconds(nextMessage.createdAt, message.createdAt) <= 20;

  const smallerMarginBottom =
    isNextMessageFromSameOwner && isNextMessageIn20Sec;

  return (
    <>
      <Row className="Row-reverse" isCurrentUser={message.fromUserId == state?.user?.id}>
        <MessageBubleWrapper
          smallerMarginTop={smallerMarginTop}
          smallerMarginBottom={smallerMarginBottom}
          isCurrentUser={message.fromUserId == state?.user?.id}
        >
          <Message>
            {message.message}
          </Message>
          <StatusContainer>
            {message.fromUserId === state?.user?.id &&
              (message.seen ? (
                <BiCheckDouble className="icon" />
              ) : message.sent ? (
                <BiCheck className="icon" />
              ) : (
                <GrUploadOption className="icon" />
              ))}
          </StatusContainer>
        </MessageBubleWrapper>
      </Row>
    </>
  );
};

export default MessageBubble;

const Message = styled.div`
  flex: 1;
  margin: 10px;
`;

const Row = styled.div<{ isCurrentUser: Boolean }>`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: ${(props) =>
    props.isCurrentUser ? "flex-end" : "flex-start"};
`;

const StatusContainer = styled.div`
  width: 18px;
  /* background-color: green; */
  height: 100%;
  position: relative;
  .icon {
    position: absolute;
    right: 0;
    bottom: 0;
  }
`;

const MessageBubleWrapper = styled.div<{
  isCurrentUser: Boolean;
  smallerMarginTop: Boolean;
  smallerMarginBottom: Boolean;
}>`
  min-width: 300px;
  max-width: 800px;
  border-radius: 10px;
  margin: 10px;
  justify-content: space-between;
  display: flex;
  flex-direction: row;

  color: ${(props) => (props.isCurrentUser ? "#ffffff" : "#000000")};
  background-color: ${(props) => (props.isCurrentUser ? "#e9506f" : "#dbdbdb")};
  ${(props) => (props.smallerMarginTop ? "margin-top: 3px;" : "")};
  ${(props) => (props.smallerMarginBottom ? "margin-bottom: 3px;" : "")};
`;
