import db from "./index";
import { faker } from "@faker-js/faker";

const seed = async () => {
  const messages = await db.message.findMany();

  for (const message of messages) {
    await db.message.update({
      where: { id: message.id },
      data: { message: faker.lorem.sentence() },
    });
  }
};

seed();
